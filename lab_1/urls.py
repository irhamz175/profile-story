from django.urls import re_path
from .views import index,gallery, kegiatan

#url for app
urlpatterns = [
    re_path(r'^$', index, name='index'),
    re_path(r'gallery', gallery, name='gallery'),
    re_path(r'kegiatan', kegiatan, name="kegiatan")
]
