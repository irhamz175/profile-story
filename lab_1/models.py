from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    hari = models.CharField(max_length=10)
    tanggal = models.CharField(max_length=50)
    jam = models.CharField(max_length=25)
    nama = models.CharField(max_length=50)
    tempat = models.CharField(max_length=50)
    kategori = models.CharField(max_length=50)
    
