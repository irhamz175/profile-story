from django.shortcuts import render
from .forms import KegiatanForm
from .models import Kegiatan

def index(request):
    return render(request, 'index.html')

def gallery(request):
    return render(request, 'gallery.html')

def kegiatan(request):
    if(request.method=="POST"):
        kegiatan_form = KegiatanForm(request.POST)
        if (kegiatan_form.is_valid()):
            kegiatan_form.save()
        else:
            pk = request.POST['id']
            obj = Kegiatan.objects.get(pk=pk)
            obj.delete()

    kegiatan_form = KegiatanForm()

    kegiatan_objects = Kegiatan.objects.all()

    context  = {
        'form': kegiatan_form,
        'objects': kegiatan_objects
    }

    return render(request, 'jadwal.html', context)
